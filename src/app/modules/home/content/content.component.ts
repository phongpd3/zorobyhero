import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'waste-zero-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  items = [
    { title: 'Slide 1' },
    { title: 'Slide 2' },
    { title: 'Slide 3' },
  ];
  constructor() { }

  ngOnInit() {
  }

}
