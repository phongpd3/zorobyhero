export const ServerMessageMap = {
  UserNameIsEmpty: 'Auth.Common.UserEmptyError',
  PasswordIsEmpty: 'Auth.Common.PasswordEmptyError',
  UserNotFoundException: 'Auth.Common.UserNotFoundError',
  NotAuthorizedException: 'Auth.Common.PasswordWrongError',
  // fix old password error notification
  OldPasswordException: 'Auth.Common.OldPasswordError',
  UserNotConfirmedException: 'Auth.Common.UserNotConfirmError',
  NetworkUnreachable: 'Common.Message.NetworkUnreachableError',
  LoginError: 'Auth.Login.LoginError',
  NameIsEmpty: 'Auth.Common.UserEmptyError',
  UsernameExistsException: 'Auth.SignUp.UserExistedError',
  UserNameInvalid: 'Auth.Common.UserInvalidError',
  InvalidPasswordException: 'Auth.Common.PasswordInvalidError',
  PasswordNotMatching: 'Auth.ChangePassword.PasswordNotMatchError',
  SignUpError: 'Auth.SignUp.SignUpError',
  UserPhoneNumberFormatException: 'SINGUP_PHONE_NUMBER_ERROR',
  // MEMBER ERROR
  'GraphQL error: You do not have permission to invite member in this group': 'MEMBER_ADD_ERROR_PERMISSION',
  'GraphQL error: User has already been invited to group': 'Member.MemberAdd.MemberAddErrorAlready',
  MemberAddInternalError: 'Common.Message.CreateFail',
  // password
  CodeMismatchException: 'Common.Message.CodeMismatchError',
  ExpiredCodeException: 'Common.Message.ExpiredCodeError', // <- error
  LimitExceededException: 'Common.Message.LimitExceededError',
  TooManyFailedAttemptsException: 'Common.Message.TooManyFailedAttemptsError',
  InvalidParameterException: "Common.Message.InvalidParameterException",
  InvalidPhoneNumberOrEmail: "Common.Message.InvalidPhoneNumberOrEmail",
  UserLambdaValidationException: "Common.Message.UserLambdaValidationException"
  // help
};
