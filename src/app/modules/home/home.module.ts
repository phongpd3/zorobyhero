import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';
import { HomePageRoutingModule } from './home-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ContentComponent } from './content/content.component';
import { CommunityComponent } from './community/community.component';
import { TrashCanComponent } from './trash-can/trash-can.component';
import { BackgroundVideoComponent } from './background-video/background-video.component';

import { CarouselComponent } from './carousel/carousel.component';
import { SharedModule } from '../../shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    HomePageRoutingModule,
    SharedModule
  ],
  declarations: [
    HomePageComponent,
    HeaderComponent,
    FooterComponent,
    ContentComponent,
    CommunityComponent,
    TrashCanComponent,
    BackgroundVideoComponent,
    CarouselComponent
  ],
  exports: [
    HomePageComponent
  ],
  entryComponents: [
    HomePageComponent
  ],
  providers: [],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class HomePageModule { }
