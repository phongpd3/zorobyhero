export const ViewImage = {
    IMAGE_TYPE: 'jpeg',
    IMAGE_CONTENT_TYPE: 'image/jpeg',
    AVATAR_DEFAULT: 'assets/images/avatar_default.png'
};
