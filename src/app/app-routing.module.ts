import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Error404Page } from './core/pages/error404/error404.page';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomePageModule)
  },
  { path: "404", component: Error404Page },
  // otherwise redirect to 404
  { path: '**', redirectTo: '/' + "404" },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
}
