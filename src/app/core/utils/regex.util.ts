// tslint:disable-next-line:max-line-length
const EMAIL_REGEX = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
// new regex check uplowcase
const All_CASE_EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PHONE_REGEX = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
const IGNORE_REGION_PHONE_REGEX = /^[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,6}$/;
// tslint:disable-next-line:max-line-length
const EMAIL_PHONE_REGEX = /(^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,14}$)|(^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$)/;
const PASSWORD_REGEX = /^(?=.*[A-Za-z])(?=.*\d)[\s\S]{6,}$/;
const NUMBER = /^[0-9]*$/;
const CODE_REGEX = /^\d+$/;
const USER_CODE_REGEX = /^[Uu]{1}(\d{7})$/;
const USER_CODE_DISPLAY_REGEX = /^[Uu]{1}-([A-Z0-9]{5})$/;
const ZIP_CODE = /^[0-9]{5}(?:-[0-9]{4})?$/;
export {
  EMAIL_REGEX,
  PHONE_REGEX,
  EMAIL_PHONE_REGEX,
  PASSWORD_REGEX,
  NUMBER,
  CODE_REGEX,
  USER_CODE_REGEX,
  IGNORE_REGION_PHONE_REGEX,
  ZIP_CODE,
  All_CASE_EMAIL_REGEX,
  USER_CODE_DISPLAY_REGEX
};
