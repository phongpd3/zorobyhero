import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { MaterialModule } from './modules/material.module';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';
import { MdePopoverModule } from '@material-extended/mde';
import { CarouselItemDirective } from './directive/carousel-item.directive';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyD-ot6wXwsPccj7Fl3ljTlkV41GfqouSxw",
      libraries: ["places"]
    })
  ],
  exports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MdePopoverModule,
    CarouselItemDirective
  ],
  declarations: [CarouselItemDirective],
  entryComponents: [],
})

export class SharedModule {
}
