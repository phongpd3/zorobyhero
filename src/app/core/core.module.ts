import { NgModule, Optional, SkipSelf, ErrorHandler, NgZone } from '@angular/core';
import { throwIfAlreadyLoaded } from './module-import-guard';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { Error404Page } from './pages/error404/error404.page';
import { Subject } from 'rxjs';
import * as _ from 'lodash';
const forceLogout$ = new Subject();

export class ZeroByHeroErrorHandler implements ErrorHandler {

  constructor() { }

  handleError(error: Error) {
    console.error("AnaGlobalErrorHandler err=", error);
    if (error && error.message && error.message.includes('NotAuthorizedException: Invalid login token. Token is expired.')) {
      forceLogout$.next();
    }
  }
}

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
  ],
  declarations: [
    Error404Page,
  ],
  exports: [
    SharedModule
  ],
  entryComponents: [
  ],
  providers: [
    { provide: ErrorHandler, useClass: ZeroByHeroErrorHandler }
  ]
})

export class CoreModule {
  constructor(
    @Optional() @SkipSelf() parentModule: CoreModule,
  ) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
