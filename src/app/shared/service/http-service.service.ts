import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { serviceAPI } from './service-api.const';
import { Observable } from 'rxjs';

const headers = new HttpHeaders({
  'Content-Type': 'application/json',
});

@Injectable()
export class HttpServices {
  urlService = serviceAPI.URL;

  constructor(private http: HttpClient) {
  }

  /**
   * Construct a GET request which interprets the body as an `ArrayBuffer` and returns it.
   *
   * @return an `Observable` of the body as an `ArrayBuffer`.
   */
  get(url: string): Observable<any> {
    if (url.includes('undefined')) {
      return new Observable<any>();
    }
    const newUrl = this.urlService + url;
    return this.http.get(newUrl, {
      headers: headers,
    });
  }

  /**
   * Construct a POST request which interprets the body as JSON and returns it.
   *
   * @return an `Observable` of the body as an `Object`.
   */
  post(url: string, body: any): Observable<any> {
    if (url.includes('undefined')) {
      return new Observable<any>();
    }
    const newUrl = this.urlService + url;
    return this.http.post(newUrl, body, {
      headers: headers,
    });
  }

  /**
   * Construct a PUT request which interprets the body as an `ArrayBuffer` and returns it.
   *
   * @return an `Observable` of the body as an `ArrayBuffer`.
   */
  put(url: string, body: any): Observable<any> {
    if (url.includes('undefined')) {
      return new Observable<any>();
    }
    const newUrl = this.urlService + url;
    return this.http.put(newUrl, body, {
      headers: headers,
    });
  }

  /**
   * Construct a DELETE request which interprets the body as JSON and returns it.
   *
   * @return an `Observable` of the body as an `Object`.
   */
  delete(url: string): Observable<any> {
    if (url.includes('undefined')) {
      return new Observable<any>();
    }
    const newUrl = this.urlService + url;
    return this.http.delete(newUrl, {
      headers: headers,
    });
  }

  /**
   * Construct a REQUEST request which interprets the body as JSON and returns it.
   *
   * @return an `Observable` of the body as an `Object`.
   */
  requests(body: any) {
    return this.http.request(body);
  }

}
